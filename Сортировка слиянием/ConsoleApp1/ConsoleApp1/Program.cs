﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {          
            int[] arr = new int[] { 1, 7, 4, 9, 3, 8, 2, 0, 5, 6 };
            Print(arr);
            Console.WriteLine();
            String s = string.Join(" ", Sort(arr));
            Console.WriteLine(s);
            Console.ReadKey();
        }

        static void Print(int[] ar)
        {
            for (int i = 0; i < ar.Length; i++)
            {
                Console.Write(" " + ar[i]);
            }
        }

        public static int[] Sort(int[] a)
        {
            if (a.Length == 1)
                return a;
            int index = a.Length / 2;
            return merge(Sort(a.Take(index).ToArray()), Sort(a.Skip(index).ToArray()));
        }

        static int[] merge(int[] a1, int[] a2)
        {
            int a = 0, b = 0;
            int[] merged = new int[a1.Length + a2.Length];
            for (int i = 0; i < a1.Length + a2.Length; i++)
            {
                if (b < a2.Length && a < a1.Length)
                    if (a1[a] > a2[b] && b < a2.Length)
                        merged[i] = a2[b++];
                    else
                        merged[i] = a1[a++];
                else
                    if (b < a2.Length)
                    merged[i] = a2[b++];
                else
                    merged[i] = a1[a++];
            }
            return merged;
        }
    }
}
