﻿/*Реализация почти всех методов стандартного Microsoft'ского LinkedList класса*/
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace ConsoleApp9
{
    class Program
    {
        //private static string xml;
        //private static string version;
        //private static object encoding;

        static void Main(string[] args)
        {
            
            //var a = new int[] { 1, 2, 3, 4, 5 };
            //MyLinkadList my = new MyLinkadList(a);
            //my.AddFirst(1);
            //my.AddFirst(2);
            //my.AddFirst(3);
            //my.AddFirst(4);
            Class1 c1 = new Class1();
            c1.X = 7;
            c1.Y = "sdsad";

            MyReverse list = new MyReverse();

            var n1 = new Node();
            var n2 = new Node();
            var n3 = new Node();
            var n4 = new Node();
            //var n5 = new Node();
            //var n6 = new Node();
            //var n7 = new Node();
            //var n8 = new Node();

            n1.Data = 1;
            n2.Data = 2;
            n3.Data = 3;
            n4.Data = 4;
            //n5.Data = 5;
            //n6.Data = 6;
            //n7.Data = 7;
            //n8.Data = 8;

            n1.Next = n2;
            n2.Next = n3;
            n3.Next = n4;
            n4.Next = null;


            //var s = JsonConvert.SerializeObject(c1);
            //const string test = @"c:\temp\example.txt";
            //File.WriteAllText(test, );
            // Десерилизация
            //    var t = File.ReadAllText(test);
            //Console.WriteLine(t);        
            //var obj = JsonConvert.DeserializeObject<Class1>(t);
            //n5.Next = n6;
            //n6.Next = n7;
            //n7.Next = n8;
            //n8.Next = null;

            list.Print1(n1);
            Console.ReadLine();
            //list.Print1(list.MyReverse1(n1));
            //list.MyReverse(n1);
            //LinkedList


            //var y = my.MyFind(3);


            //my.AddAfter(y,8);
            //var n = my.MyBeforeFind(y);
            //Console.ReadLine();
            //my.Print();

            //my.RemoveBefore(y);
            //my.Print();
            ////my.MyReverse();
            //my.Print();
            //Console.WriteLine(my.First.Data);
            //////Console.WriteLine(my.Count);
            //Console.ReadLine();
            //my.RemoveFirst();
            //my.Print();
            //Console.ReadLine();
            //my.RemoveLast();
            //my.Print();

            //Console.ReadLine();
            //my.RemoveLast();
            //my.Print();
            ////my.Print2();
            //Console.ReadLine();
            //my.RemoveLast();
            //my.Print();

            Console.ReadKey();
        }

        private static void ReadXmlFile(string v)
        {
            throw new NotImplementedException();
        }

        public class MyReverse
        {
            public Node MyReverse1 (Node list)
            {
                Node n = null;
                Node p = null;
                Node l = list;

                while (l != null)
                {
                    n = l.Next;
                    l.Next = p;
                    p = l;
                    l = n;
                }
                return p ;
                
            }

            public void Print1(Node n1)
            {
                Node x = n1;
                while (x != null)
                {
                    Console.WriteLine(x.Data);
                    x = x.Next;
                }
            }

        }
        


        public class MyLinkadList
        {
            
            public Node First;
            public Node Last;
            private int count = 0;

            public MyLinkadList()
            {

            }

            public MyLinkadList(IEnumerable<int> collection)
            {
              
                foreach (int element in collection)
                {
                    AddLast(element);                 
                }
            }

            public void AddFirst(int data)
            {

                count++;
                Node a = new Node();
                a.Data = data;
                if (First == null)
                {
                    a.Next = null;
                    First = a;
                    Last = a;
                }
                else
                {
                    a.Next = First;
                    First = a;
                }
               
            }

            public void AddAfter (Node y, int data)
            {   

                Node a = new Node();
                a.Data = data;
                a.Next = y.Next;
                y.Next = a;               
            }

            public void AddBefore(Node y, int data)
            {
                    Node a = new Node();
                    a.Data = data;
                    a.Next = y.Next;
                    y.Next = a;
                

            }
            public void RemoveAfter(Node y)
            {
                if (y == null)
                {
                    return;
                }
                
                else if (y.Next == null)
                {
                        MessageBox.Show("Следующий элемент нельзя удалить так как его нет",
                       "Ошибка",
                       MessageBoxButtons.OKCancel);
                }
                else
                {
                    var z = y.Next;
                    y.Next = y.Next.Next;
                    z.Next = null;
                }

            }

            //public void RemoveBefore (Node y)
            //{
            //    var x = MyBeforeFind1(y);

            //    if ( x== y)
            //    {
            //        MessageBox.Show("Предыдущий элемент нельзя удалить так как его нет",
            //           "Ошибка",
            //           MessageBoxButtons.OKCancel);
            //        return;
            //    }

            //    if (x.Next == y)

            //    {
            //        First = x.Next ;

            //    }
            //    else
            //    {
                    
            //        x.Next.Next = null;
            //        x.Next = y;
            //    }
            //}

            public void AddLast(int data)
                {
                count++;
                Node a = new Node();
                a.Data = data; 
                if (Last == null)
                {
                    a.Next = null;
                    First = a;
                    Last = a;
                }
                else
                {
                    a.Next = null;
                    Last.Next = a;
                    Last = a;
                }
                }

            public void Clear()
            {
                First = null;
                Last = null;
                count = 0;

            }

            public Node MyFind (int x)
            {
                Node y = First;
                while (y != null)
                {
                    if (y.Data == x)
                    {
                        return y;
                    }
                    y = y.Next;
                }
                return y;
            }

            public void RemoveFirst()
            {
               
                if (First == null)
                {
                    return;
                }
                else if (First == Last)
                {
                    First = null;
                    Last = null;
                }
                else
                {
                    First = First.Next;
                }
              
            }

            //public void RemoveLast()
            //{
            //    if (Last == null)
            //    {
            //        return;
            //    }
            //    else if (Last == First )
            //    {
            //        First = null;
            //        Last = null;
            //    }
            //    else
            //    {
            //      var r = ВернутьПредпоследнийЭлемент();
            //        Last = r;
            //        r.Next = null;
            //    }
            //}

            //public void MyReverse()
            //{
            //    Node helpNode = new Node();
            //    Node newFirst = null;

            //    while ((helpNode = First)!=null)
            //    {
            //        First = First.Next;
            //        helpNode.Next = newFirst;
            //        newFirst = helpNode;
            //    }
            //    First = newFirst;
            //}
            
            public bool MyContains (int x)
            {
                Node y = First;
                while (y !=null)
                {
                    if (y.Data == x)
                    {                     
                        return true;
                    }
                        y = y.Next;                    
                }
                return false;
            }

            public int MyContainsCount (int x)
            {
                Node y = First;
                int n = 0;
                while (y != null)
                {
                    if (y.Data == x)
                    {
                        n++;
                    }
                    y = y.Next;
                }
                return n;
            }


            public void Print()
            {
                Node x = First;
                while (x != null)
                {
                    Console.WriteLine(x.Data);
                    x = x.Next;                    
                }
            }

            private Node ВернутьПредпоследнийЭлемент()
            {
                Node y = First;
                while (y.Next.Next != null)                    
                {                 
                        y = y.Next;
                }               
                return y;
            }

            public Node MyBeforeFind(Node n)
            {
                Node y = First;              
                while (y.Next != n)
                {
                     y = y.Next;
                }
                return y;
            }

            private Node MyBeforeFind1(Node n)
            {
                Node y = First;
                if (First == n)
                {
                    return y;
                }
                if (First.Next == n)
                {
                    return y;
                }
                else
                {
                    while (y.Next.Next != n)
                    {
                        y = y.Next;
                    }
                    return y;
                }
            }

            public int Count
            {              
                get
                {                   
                    return count; 
                }
            }
           
        }
    }


    public class Node
    {
        public int Data;
        public Node Next;
    }
}