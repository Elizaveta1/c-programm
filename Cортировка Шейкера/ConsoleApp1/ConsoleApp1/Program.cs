﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Sort();
            Console.ReadKey();
        }

        static void Sort()
        {
            int[] myint = {5, 2, 3, 1,12,7,1,8,4,10 };

            PrintArray(myint);
            ShakerSort(myint);
            PrintArray(myint);

            Console.ReadLine();
        }

       
        static void ShakerSort(int[] ar )
        {
            int left = 0;
            int right = ar.Length - 1;
            int count = 0;

            while (left < right)
            {
                for (int i = left; i < right; i++)
                {
                    count++;
                    if (ar[i] > ar[i + 1])
                        Swap(ar, i, i + 1);
                }
                right--;

                for (int i = right; i > left; i--)
                {
                    count++;
                    if (ar[i - 1] > ar[i])
                        Swap(ar, i - 1, i);
                }
                left++;
            }
            Console.WriteLine("\nКоличество сравнений = "+ count);
            
        }

        //Меняем элементы местами.
        static void Swap(int[] myar, int i, int j)
        {
            int it = myar[i];
            myar[i] = myar[j];
            myar[j] = it;
        }

        //Выводим массив
        static void PrintArray(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(" " + a[i] + " ");
            }           
        }
    }
}
