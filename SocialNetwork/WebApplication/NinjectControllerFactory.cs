﻿using BusinessLogic.Implementations;
using BusinessLogic.Interface;
using Domain;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Web
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory ()
        {
            ninjectKernel = new StandardKernel();
            AddBindings(); 
        }
        protected override IController GetControllerInstance (System.Web.Routing.RequestContext requestContext, Type controllerType )
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings ()
        {
            ninjectKernel.Bind<IUsersRepository>().To<EFUsersRepository>();
            ninjectKernel.Bind<IFriendsRepository>().To<EFFriendsRepository>();
            ninjectKernel.Bind<IFriendRequestsRepository>().To<EFFriendRequestRepository>();
            ninjectKernel.Bind<IMessagesRepository>().To<EFMessagesRepository>();
            ninjectKernel.Bind<EFDbContext>().ToSelf().WithConstructorArgument("connectionString", 
                                                                               ConfigurationManager.ConnectionStrings[0]
                                                                               .ConnectionString);
            ninjectKernel.Inject(Membership.Provider);

        }
    }
}