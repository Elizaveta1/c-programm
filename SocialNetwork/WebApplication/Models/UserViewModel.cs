﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class UserViewModel
    {
        //Пользователь
        public User User { get; set; }

        // Пользователь это моя страница
        public bool UserIsMe { get; set; }

        //Этот пользователь мой друг
        public bool UserIsMyFriend { get; set; }
        
        //Возможный друг
        public bool FriendRequestIsSent { get; set; }

        
    }
}