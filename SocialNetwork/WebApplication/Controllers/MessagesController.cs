﻿using BusinessLogic;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    public class MessagesController : Controller
    {
        private DataManager dataManager;
        public MessagesController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }
        

        public ActionResult Index()
        {
            //Готовим входящие сообщения и авторов и передаем их в ViewModel а далее в представление
            List<IncomingMessageViewModel> model = new List<IncomingMessageViewModel>();
            foreach (IncomingMessage message  in dataManager.Messages.GetIncomingMessagesByUserId((int)Membership.GetUser().ProviderUserKey))
            {
                model.Add(new IncomingMessageViewModel
                {
                    Message = message,
                    UserForm = dataManager.Users.GetUserById(message.UserFromId)
                });
            }
            return View(model);
        }

        public ActionResult Outgoing ()
        {
            //Подготавливаем исходящие сообщения и адресатов
            List<OutgoingMessageViewModel> model = new List<OutgoingMessageViewModel>();
            foreach(OutgoingMessage message in dataManager.Messages.GetOutgoingMessagesByUserId((int)Membership.GetUser().ProviderUserKey))
            {
                model.Add(new OutgoingMessageViewModel
                {
                    Message = message,
                    UserTo = dataManager.Users.GetUserById(message.UserToId)
                });
            }
                return View(model);
        }

        public ActionResult NewMessage(int userToId)
        {
            //Определяем автора сообщения,адресата и дату создания

            return View(new OutgoingMessage
                        { UserId= (int)Membership.GetUser().ProviderUserKey,
                        UserToId =userToId,
                        CreatedDate =DateTime.Now});
        }

        [HttpPost]
        public ActionResult NewMessage(OutgoingMessage message)
        {
            if(ModelState.IsValid)
            {
                dataManager.Messages.SaveOutgoingMessage(message);
                dataManager.Messages.SaveIncomingMessage(new IncomingMessage
                                                                    {
                                                                        UserId = message.UserToId,
                                                                        UserFromId = message.UserId,
                                                                        CreatedDate = DateTime.Now,
                                                                        Text = message.Text

                                                                    });
                return RedirectToAction("Index", "Home");
            }
            return View(message);
        }
    }
}