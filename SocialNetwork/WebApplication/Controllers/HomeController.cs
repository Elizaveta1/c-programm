﻿using BusinessLogic;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private DataManager dataManager;
        public HomeController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        public ActionResult Index(int id=0)
        {
            //если в адрес не был представлен Id пользователь, то вычисляем и добовляем в адресс вычесленный Id перенаправляя пользователя 
            if (id == 0)
                return RedirectToAction("Index", new { id = Membership.GetUser().ProviderUserKey });

            User user = dataManager.Users.GetUserById(id);

            // исходя из Id определяем чья страница

            UserViewModel model = new UserViewModel
            {
                User = user,
                UserIsMe = id == (int)Membership.GetUser().ProviderUserKey,
                UserIsMyFriend = dataManager.Friends.UserAreFriends((int)Membership.GetUser().ProviderUserKey, user.Id),
                FriendRequestIsSent = dataManager.FriendRequests.RequestIsSent(user.Id, (int)Membership.GetUser().ProviderUserKey)
            };
            return View(model);
        }
    }
}