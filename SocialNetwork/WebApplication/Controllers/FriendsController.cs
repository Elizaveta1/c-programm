﻿using BusinessLogic;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    public class FriendsController : Controller
    {
        private DataManager dataManager;
        public FriendsController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        public ActionResult Index()
        {
            int currentUserId = (int)Membership.GetUser().ProviderUserKey;
            FriendsViewModel model = new FriendsViewModel();

            //Выбираем всех пользователей
            IEnumerable<User> allUser = dataManager.Users.GetUsers();

            //Все заявки в друзья(Входящие и Исходящие)
            IEnumerable<FriendRequest> allRequests =
                dataManager.FriendRequests.GetFriendRequests().Where(
                                        x =>
                                        x.UserId == currentUserId ||
                                        x.PossibleFriendId == currentUserId);
            //Из всех заявок выбираем Входящие
            IEnumerable<FriendRequest> IncomingRequests = allRequests.Where(x => x.UserId == currentUserId);

            //Из всех заявок выбираем Входящие
            IEnumerable<FriendRequest> OutgoingRequests = allRequests.Where(x => x.PossibleFriendId == currentUserId);

            // Из входящих заявок выбираем User'ов и передаем их в модель
            model.IncomingRequests = (from aU in allUser
                                      from iR in IncomingRequests
                                      where aU.Id == iR.PossibleFriendId
                                      select aU);
            //Тоже самое с исходящими заявками
            model.OutgoingRequests = (from aU in allUser
                                      from oR in OutgoingRequests
                                      where aU.Id == oR.UserId
                                      select aU);
            //Выбираем все записи о друзьях
            IEnumerable<Friend> allFriends = dataManager.Friends.GetFriends().Where(x => x.FriendId == currentUserId);

            //По Id передаем  User'ов в модель
            model.Friends = (from aU in allUser
                             from aF in allFriends
                             where aU.Id == aF.UserId
                             select aU);
            return View(model);
        }

        //заявка другому пользователю
        public ActionResult AddFriendRequest(int id)
        {
            FriendRequest friendRequest = new FriendRequest
            {
                UserId = id,
                PossibleFriendId = (int)Membership.GetUser().ProviderUserKey

            };
            dataManager.FriendRequests.AddFriendRequest(friendRequest);

            return RedirectToAction("Index", "Home", new { id });
        }

        //Пользователь отменил заявку в друзья

        public ActionResult ConcelFriendRequest (int id)
        {
            dataManager.FriendRequests.DeleteFriendRequest(
                dataManager.FriendRequests.GetFriendRequests().FirstOrDefault(
                    x => x.UserId == id && x.PossibleFriendId == (int)Membership.GetUser().ProviderUserKey));

            return RedirectToAction("Index", "Home", new { id });
        }

        //Пользователь откланил заявку от другого пользователя

        public ActionResult DisagreeFriendRequest (int id)
        {
            dataManager.FriendRequests.DeleteFriendRequest(
                dataManager.FriendRequests.GetFriendRequests().FirstOrDefault
                (x => x.UserId == (int)Membership.GetUser().ProviderUserKey &&
                    x.PossibleFriendId ==id));

            return RedirectToAction("Index", "Home", new { id });
        }

        //Пользователь подтвердил заявку в друзья
        public ActionResult ConfirmFriendRequest(int id)
        {
            // Создаем дублирующую запись для обоих пользователей
            Friend friend1 = new Friend { UserId = (int)Membership.GetUser().ProviderUserKey, FriendId = id };
            Friend friend2 = new Friend { UserId = id, FriendId = (int)Membership.GetUser().ProviderUserKey };
            dataManager.Friends.AddFriend(friend1);
            dataManager.Friends.AddFriend(friend2);
            // Удаляем соответстыующую заявку в друзья
            dataManager.FriendRequests.DeleteFriendRequest(
                dataManager.FriendRequests.GetFriendRequests().FirstOrDefault(
                    x => 
                    (x.UserId == id && 
                    x.PossibleFriendId == 
                    (int)Membership.GetUser().ProviderUserKey) || 
                    (x.UserId == 
                    (int)Membership.GetUser().ProviderUserKey && 
                    x.PossibleFriendId==id)));

            return RedirectToAction("Index", "Home", new { id });
        }

        //Пользователь удалил из друзей
        public ActionResult DeleteFriend (int id)
        {
            // удвляем дублирующую запись для обоих пользователей из предыдущего действия
            int currentUserId = (int)Membership.GetUser().ProviderUserKey;

            dataManager.Friends.DeleteFriend(
                dataManager.Friends.GetFriends().FirstOrDefault(
                    x => x.UserId == id && x.FriendId == currentUserId));

            dataManager.Friends.DeleteFriend(
                dataManager.Friends.GetFriends().FirstOrDefault(
                    x => x.UserId == currentUserId && x.FriendId == id));

            return RedirectToAction("Index", "Home", new { id = currentUserId });
        }
    }

}