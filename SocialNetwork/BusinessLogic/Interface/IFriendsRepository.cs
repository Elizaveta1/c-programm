﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLogic.Interface
{
    public interface IFriendsRepository
    {
        IEnumerable<Friend> GetFriends();
        //Проверка являются ли два пользователя друзьями
        bool UserAreFriends(int userId, int user2Id);
        void AddFriend(Friend friend);
        void DeleteFriend(Friend friend);
    }
}
