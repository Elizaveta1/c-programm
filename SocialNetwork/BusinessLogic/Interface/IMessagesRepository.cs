﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interface;
using Domain;
using Domain.Entities;

namespace BusinessLogic.Interface
{
    public interface IMessagesRepository
    {
        #region Входящие сообщения

        IEnumerable <IncomingMessage> GetIncomingMessages();
        IEnumerable <IncomingMessage> GetIncomingMessagesByUserId( int userId);
        void SaveIncomingMessage(IncomingMessage message);
        void DeleteIncomingMessage(IncomingMessage message);

        #endregion

        #region Изходящие сообщения

        IEnumerable<OutgoingMessage> GetOutgoingMessages();
        IEnumerable<OutgoingMessage> GetOutgoingMessagesByUserId(int userId);
        void SaveOutgoingMessage(OutgoingMessage message);
        void DeleteOutgoingMessage (OutgoingMessage message);

        #endregion
    }
}
