﻿using BusinessLogic.Interface;
using Domain;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Data.Entity;



namespace BusinessLogic.Implementations
{
    public class EFUsersRepository : IUsersRepository
    {
        private EFDbContext _context;
        public EFUsersRepository(EFDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<User> GetUsers()
        {
            return _context.Users;
        }

        public User GetUserById(int id)
        {
            return _context.Users.FirstOrDefault(x => x.Id == id);
        }

        public User GetUserByName(string userName)
        {
            return _context.Users.FirstOrDefault(x => x.UserName == userName);
        }

        public MembershipUser GetMembershipUserByName(string userName)
        {
           User user = _context.Users.FirstOrDefault(x => x.UserName == userName);
            if (user != null)
            {
                return new MembershipUser("CustomMembershipProvider",
                    user.UserName,
                    user.Id,
                    user.Email,
                    "",
                    null,
                    true,
                    false,
                    user.CreatedDate,
                    DateTime.Now,
                    DateTime.Now,
                    DateTime.Now,
                    DateTime.Now
                    );
            }
            return null;
        }

        public string GetUserNameByEmail(string email)
        {
            User  user = _context.Users.FirstOrDefault(x => x.Email == email);
            return user != null ? user.UserName : "";
        }

        public void CreateUser(string userName, string password, string email, int numberPhone, string firstName, string lastName, string middleName)
        {
            User user = new User
            {
                UserName = userName,
                Email = email,
                Password = password,
                CreatedDate = DateTime.Now,
                FirstName = firstName,
                LastName = lastName,
                MiddleName = middleName,
                NumberPhone = numberPhone
            };

            SaveUser(user);

        }
        public bool ValidateUser(string userName, string password)
        {
            User user = _context.Users.FirstOrDefault(x => x.UserName == userName);
            if (user != null && user.Password == password)
                return true;
            return false;

        }

        public void SaveUser(User user)
        {
            if (user.Id == 0)
                _context.Users.Add(user);
            else
            {
                _context.Entry(user).State = EntityState.Modified;
            }
            _context.SaveChanges();
        }
    }
}
