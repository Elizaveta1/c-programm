﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interface;
using Domain;
using Domain.Entities;

namespace BusinessLogic.Implementations
{
    public class EFMessagesRepository : IMessagesRepository
    {
        private EFDbContext _context;
        public EFMessagesRepository(EFDbContext context)
        {
            this._context = context;
        }

        #region Входящие сообщения
        public IEnumerable<IncomingMessage> GetIncomingMessages()
        {
            return _context.IncomingMessages;
        }

        public IEnumerable<IncomingMessage> GetIncomingMessagesByUserId(int userId)
        {
            return _context.IncomingMessages.Where(x => x.UserId == userId);
        }

        public void SaveIncomingMessage(IncomingMessage message)
        {
            if (message.Id == 0)
                _context.IncomingMessages.Add(message);
            else
                _context.Entry(message).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public void DeleteIncomingMessage(IncomingMessage message)
        {
            _context.IncomingMessages.Remove(message);
            _context.SaveChanges();
        }
        #endregion

        #region Изходящие сообщения

        public IEnumerable<OutgoingMessage> GetOutgoingMessages()
        {
            return _context.OutgoingMessages;
        }

        public IEnumerable<OutgoingMessage> GetOutgoingMessagesByUserId(int userId)
        {
            return _context.OutgoingMessages.Where(x => x.UserId == userId);

        }

        public void SaveOutgoingMessage(OutgoingMessage message)
        {
            if (message.Id == 0)
                _context.OutgoingMessages.Add(message);
            else
                _context.Entry(message).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public void DeleteOutgoingMessage(OutgoingMessage message)
        {
            _context.OutgoingMessages.Remove(message);
            _context.SaveChanges();
        }
        #endregion
    }

}
