﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interface;
using Domain;
using Domain.Entities;

namespace BusinessLogic.Implementations
{
    public class EFFriendsRepository : IFriendsRepository
    {
        private EFDbContext _context;
        public EFFriendsRepository(EFDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<Friend> GetFriends()
        {
            return _context.Friends;
        }

        public bool UserAreFriends(int userId, int user2Id)
        {
            return _context.Friends.Count(x => x.UserId == userId && x.FriendId == user2Id) != 0;
        }

        public void AddFriend(Friend friend)
        {
            _context.Friends.Add(friend);
            _context.SaveChanges();
        }

        public void DeleteFriend(Friend friend)
        {
            _context.Friends.Remove(friend);
            _context.SaveChanges();
        }
       
    }
}
