﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interface;
using Domain;
using Domain.Entities;

namespace BusinessLogic.Implementations
{
    public class EFFriendRequestRepository : IFriendRequestsRepository
    {
        private EFDbContext _context;
        public EFFriendRequestRepository (EFDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<FriendRequest> GetFriendRequests()
        {
            return _context.FriendRequests;
        }

        public bool RequestIsSent(int userFromId, int userToId)
        {
            return _context.FriendRequests.Count(x => x.UserId == userFromId && x.PossibleFriendId == userToId) != 0;
        }

        public void AddFriendRequest(FriendRequest friendRequest)
        {
            _context.FriendRequests.Add(friendRequest);
            _context.SaveChanges();
           
        }

        public void DeleteFriendRequest(FriendRequest friendRequest)
        {
            _context.FriendRequests.Remove(friendRequest);
            _context.SaveChanges();
           
        }
    }

}
