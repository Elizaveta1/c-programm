﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Domain.Entities;

namespace Domain
{
    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class EFDbContext : DbContext
    {
        public EFDbContext (string connectionString)
        {
            Database.Connection.ConnectionString = "Server=127.0.0.1;Database=mydb;Uid=root;Pwd=root;";
           //Database.Connection.ConnectionString = connectionString;
        }

        public DbSet<User> Users { get; set; }
        public DbSet <Friend> Friends { get; set; }
        public DbSet <FriendRequest> FriendRequests { get; set; }
        public DbSet <IncomingMessage> IncomingMessages { get; set; }
        public DbSet <OutgoingMessage> OutgoingMessages { get; set; }
    }   
}
