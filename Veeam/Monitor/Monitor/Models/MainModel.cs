﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor.Models
{
    class MainModel
    {
        public string ProcessName;
        public uint MaxLifeTime;
        public uint CheckTime;
        public bool IsError;
    }
}
