﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Monitor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(args.Length);
            //foreach(var item in args)
            //{
            //    Console.WriteLine(item);
            //}
            var model = ArgsProcessor.Process(args);
            if (model.IsError)
            {
                Console.ReadKey();
                return;
            }

            var lastCheck = DateTime.Now;
            var monitor = new ProcessMonitor(model);
            while(true)
            {
                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                    {
                        Console.WriteLine("Good bye");
                        Thread.Sleep(2000);
                        return;
                    }
                }

                var time = DateTime.Now - lastCheck;
                if (time.TotalMinutes >= model.CheckTime)
                {
                    monitor.CheckProcesses();
                    lastCheck = DateTime.Now;
                }


            }
        }
    }
}
