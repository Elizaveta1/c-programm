﻿using Monitor.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor
{
    class ProcessMonitor
    {
        private readonly MainModel _model = null;
        private int _checkCount = 0;

        public ProcessMonitor(MainModel model)
        {
            _model = model;
        }

        public void CheckProcesses()
        {
            //Console.WriteLine("I am checking " + _checkCount++);
            var procs = Process.GetProcesses(".").Where(x => x.ProcessName.ToLower() == _model.ProcessName.ToLower()).ToList();
            foreach(var proc in procs)
            {
                var time = DateTime.Now - proc.StartTime;
                if (time.TotalMinutes >= _model.MaxLifeTime)
                {
                    proc.Kill();
                }
            }
        }

    }
}
