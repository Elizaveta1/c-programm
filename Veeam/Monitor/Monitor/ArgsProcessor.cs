﻿using Monitor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor
{
    static class ArgsProcessor
    {
        public static MainModel Process(string[] args)
        {
            var ret = new MainModel();
            if (args.Length != 3)
            {
                Console.WriteLine("You should input 3 agrs");
                ret.IsError = true;
                return ret;
            }
            ret.ProcessName = args[0];
            uint second = 0;
            if (!uint.TryParse(args[1], out second))
            {
                Console.WriteLine("Second argument should be a positive number");
                ret.IsError = true;
                return ret;
            }
            if (second == 0)
            {
                Console.WriteLine("Second argument should be more then 0");
                ret.IsError = true;
                return ret;
            }
            ret.MaxLifeTime = second;

            uint third = 0;
            if (!uint.TryParse(args[2], out third))
            {
                Console.WriteLine("Third argument should be a positive number");
                ret.IsError = true;
                return ret;
            }
            if (third == 0)
            {
                Console.WriteLine("Third argument should be more then 0");
                ret.IsError = true;
                return ret;
            }
            ret.CheckTime = third;
            return ret;
        }
    }
}
