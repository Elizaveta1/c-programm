﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace CareersChecking
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://careers.veeam.com/");
            driver.Manage().Window.Maximize();
            var country = driver.FindElement(By.Id("country-element"));
            country.Click();

            var contaryName = comboBox1.Text;
            var sc = country.FindElement(By.ClassName("scroller-content"));
            var list = sc.FindElements(By.ClassName("selecter-item"));
            var rom = list.Where(x => x.Text == contaryName).FirstOrDefault();
            rom.Click();

            var selectedLangList = new List<string>();
            if(checkBox1.Checked)
            {
                selectedLangList.Add(checkBox1.Text);
            }
            if (checkBox2.Checked)
            {
                selectedLangList.Add(checkBox2.Text);
            }

            var lang = driver.FindElement(By.Id("language"));
            lang.Click();

            foreach(var lngName in selectedLangList)
            {
                var langList = driver.FindElements(By.ClassName("controls-checkbox"));
                var eng = langList.Where(x => x.Text == lngName).FirstOrDefault();
                eng.Click();
            }
            

            Thread.Sleep(1000);
            var jobFound = driver.FindElement(By.CssSelector(".pb15-sm-down.mb30-md-up.text-center-md-down")).Text;
            var jobStr = jobFound.Split(' ')[0];

            var checkValue = uint.Parse(textBox1.Text);
            var resValue = uint.Parse(jobStr);

            if (resValue == checkValue)
            {
                label1.Text = "Test successful";
            }
            else
            {
                label1.Text = "Test failed " + jobFound + " but checkValue was " + checkValue;
            }

            Thread.Sleep(5000);
            driver.Quit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }
    }
}
