﻿/*
 *Линейный, последовательный поиск
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] ar = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Print (ar);
            Console.WriteLine();
            Console.WriteLine("Введите число для поиска");
            int p = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Poisk(ar,p));
            Console.ReadKey();
        }

        static string Poisk(int[] ar, int p)
        {
            string st = "Ваше число не найдено! ";

            for (int i = 0; i < ar.Length - 1; i++)
            {
                if (ar[i] == p)
                    st = ("Ваше число " + p + " найдено! Его индекс = " + i);
            }
            return st;
        }

        static void Print (int[] ar)
        {
            for (int i = 0; i < ar.Length; i++)
            {
                Console.Write(" " + ar[i]);
            }
        }
    }
}
