﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] ar = new int[10] { 1, 4, 3, 7, 9, 12, 6, 11, 9, 10 };
            Print(ar);
            Sort(ar);
            Print(ar);
            Console.WriteLine("Введите число для поиска ");
            int p = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine( Poisk(ar, p));
            Console.ReadKey();
        }

        static void Sort (int[] ar)
        {
        int j = 0;
        int a;
            while (j<ar.Length)
            {
                int i = 0;
                for (i = 0; i<ar.Length - 1; i++)
                {
                    if (ar[i] > ar[i + 1])
                    {
                        a = ar[i];
                        ar[i] = ar[i + 1];
                        ar[i + 1] = a;
                        i++;
                        a = 0;
                    }
                }
            j++;
            }
        }
        static void Print(int[] ar)
        {
            for (int i = 0; i < ar.Length; i++)
            {
                Console.Write(" " + ar[i]);
            }
            Console.WriteLine();
        }

        static string Poisk (int[] ar, int p)
        {
            int left = 0; // Левая граница 
            int right = ar.Length - 1; // Правая граница
            int index = 0; //середина
            string res = "Такого элемента нет в массиве";
            while (left <= right)
            {
                index = (right + left) / 2;
                if (ar[index] == p)
                {
                    res = "Искомый элемент найден! Индекс:= " + index;
                    break;
                }
                // Меняем границы.
                if (ar[index] < p)
                {
                    left = index + 1;
                }
                else
                {
                    right = index - 1;
                }
            }

            return res;
        }
    }  
}
