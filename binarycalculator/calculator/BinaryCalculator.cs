﻿/* Класс BinaryCalculator предназначен для расчета двоичных чисел любой размерности !
 Это учебный проект! Я понимаю что использование  Листов и Строк, не самое эфективное решение. 
 Моя задача была попрактиковаться и продумать логику.
 В будующем я перепишу данный проет используя более эфективные  структуры.  */

using System;
using System.Collections.Generic;

namespace Calculator
{
    public class BinaryCalculator
    {

        public string Addition(string variable1, string variable2)
        {
            int chisloPerehoda = 0;
            List<char> list = new List<char>();
            if (variable1.Length != variable2.Length)
            {
                while (variable1.Length != variable2.Length)
                {
                    if (variable1.Length > variable2.Length)
                    {
                        variable2 = variable2.Insert(0, "0");
                    }
                    else if (variable1.Length < variable2.Length)
                    {
                        variable1 = variable1.Insert(0, "0");
                    }
                }
                for (int i = variable1.Length - 1; i >= 0; i--)
                {
                    if (variable1[i] == '1' && variable2[i] == '1')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 1;
                        }
                        else
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 1;
                        }
                    }
                    else if (variable1[i] == '1' && variable2[i] == '0')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 0;
                        }
                        else
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 1;
                        }
                    }
                    else if (variable1[i] == '0' && variable2[i] == '1')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 0;
                        }
                        else
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 1;
                        }
                    }
                    else if (variable1[i] == '0' && variable2[i] == '0')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 0;
                        }
                        else
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 0;
                        }
                    }
                }
                if (chisloPerehoda == 1)
                {
                    list.Insert(0, '1');
                }
            }

            else if (variable1.Length == variable2.Length)
            {
                for (int i = variable1.Length - 1; i >= 0; i--)
                {
                    if (variable1[i] == '1' && variable2[i] == '1')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 1;
                        }
                        else
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 1;
                        }
                    }
                    else if (variable1[i] == '1' && variable2[i] == '0')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 0;
                        }
                        else
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 1;
                        }
                    }
                    else if (variable1[i] == '0' && variable2[i] == '1')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 0;
                        }
                        else
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 1;
                        }
                    }
                    else if (variable1[i] == '0' && variable2[i] == '0')
                    {
                        if (chisloPerehoda == 0)
                        {
                            list.Insert(0, '0');
                            chisloPerehoda = 0;
                        }
                        else
                        {
                            list.Insert(0, '1');
                            chisloPerehoda = 0;
                        }
                    }
                }
                if (chisloPerehoda == 1)
                {
                    list.Insert(0, '1');
                }
            }
            char[] summa = list.ToArray();
            string rezyltat = new string(summa);
            return rezyltat;
  
        }

        public string Subtraction(string variable1, string variable2)
        {
            int dec = Convert.ToInt32(variable1, 2);
            int dec1 = Convert.ToInt32(variable2, 2);
            int b = 0;
            List<char> list = new List<char>();
            if (dec >= dec1)
            {
                if (variable1.Length != variable2.Length)
                {
                    while (variable1.Length != variable2.Length)
                    {
                        if (variable1.Length > variable2.Length)
                        {
                            variable2 = variable2.Insert(0, "0");
                        }
                        else if (variable1.Length < variable2.Length)
                        {
                            variable1 = variable1.Insert(0, "0");
                        }
                    }
                    for (int i = variable1.Length - 1; i >= 0; i--)
                    {

                        if (variable1[i] == '1' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                        }
                        else if (variable1[i] == '1' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 1;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                        }
                    }
                    if (b == 1)
                    {
                        list.Insert(0, '1');
                    }
                }
                else if (variable1.Length == variable2.Length)
                {
                    for (int i = variable1.Length - 1; i >= 0; i--)
                    {

                        if (variable1[i] == '1' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                        }
                        else if (variable1[i] == '1' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 1;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                        }
                    }
                    if (b == 1)
                    {
                        list.Insert(0, '1');
                    }
                }
            }
            else if (dec < dec1)
            {
                if (variable1.Length != variable2.Length)
                {
                    while (variable1.Length != variable2.Length)
                    {
                        if (variable1.Length > variable2.Length)
                        {
                            variable2 = variable2.Insert(0, "0");
                        }
                        else if (variable1.Length < variable2.Length)
                        {
                            variable1 = variable1.Insert(0, "0");
                        }
                    }
                    for (int i = variable1.Length - 1; i >= 0; i--)
                    {

                        if (variable1[i] == '1' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                        }
                        else if (variable1[i] == '1' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                        }
                    }
                    if (b == 1)
                    {
                        list.Insert(0, '1');
                    }
                }
                else if (variable1.Length == variable2.Length)
                {
                    for (int i = variable1.Length - 1; i >= 0; i--)
                    {

                        if (variable1[i] == '1' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                        }
                        else if (variable1[i] == '1' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '1')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                            else
                            {
                                list.Insert(0, '0');
                                b = 1;
                            }
                        }
                        else if (variable1[i] == '0' && variable2[i] == '0')
                        {
                            if (b == 0)
                            {
                                list.Insert(0, '0');
                                b = 0;
                            }
                            else
                            {
                                list.Insert(0, '1');
                                b = 1;
                            }
                        }
                    }
                    

                    if (b == 1)
                    {
                        list.Insert(0, '1');
                    }
                    
                }
                list.Insert(0, '-');
            }

            while (list.Count > 1 && list[0] == '0')
            {
                list.RemoveAt(0);
            }

            char[] ccc = list.ToArray();
            string c = new string(ccc);
            return c;

        }

        public string Multiplication(string variable1, string variable2)
        {
            string rezyltat = "0";
            int n = 0;
            string st;
            List<string> list = new List<string>();
            for (int i = variable1.Length - 1; i >= 0; i--)
            {
                if (n == 0)
                {
                    if (variable1[i] == '1')
                    {
                        rezyltat = Addition(rezyltat, variable2);
                    }
                    if (variable1[i] == '0')
                    {
                        st = "    ";
                    }
                    n++;
                }
                else if (n > 0)
                {
                    if (variable1[i] == '1')
                    {
                        st = AddZeros(variable2, n);
                        rezyltat = Addition(rezyltat, st);
                    }
                    if (variable1[i] == '0')
                    {
                        st = "     ";
                    }
                    n++;
                }
            }
            return rezyltat;
        }

        static string AddZeros(string s, int n)
        {
            while (n > 0)
            {
                s = s + "0";
                n--;
            }
             return s;
        }

        public DivResult division1(string variable1, string variabli2)
        {
            var ret = new DivResult();
            string workstring;
            string ss = "";
            string result = "";
            variabli2 = RemoverZeros(variabli2);
            string constantstring = variabli2;
            workstring = Comparison(variable1, constantstring);
            if (workstring == "less")
            {
                result += "0";
                ret.Result = result;
                ret.Reminder = variable1;
                return ret;
            }

            while (workstring == "more" || workstring == "equally")
            {
                ss = constantstring;
                constantstring += "0";
                workstring = Comparison(variable1, constantstring);
            }
            variable1 = Subtraction(variable1, ss);
            result += "1";
            while (ss != variabli2)
            {
                ss = ss.Remove(ss.Length - 1);
                workstring = Comparison(ss, variable1);
                if (workstring == "more")
                {
                    result += "0";
                }
                else
                {
                    variable1 = Subtraction(variable1, ss);
                    result += "1";
                }
            }

            ret.Result = result;
            ret.Reminder = variable1;

            return ret;
        }

        public string Comparison(string s, string s1)
        {
            int n = 0;
            int i = 0;
            int j = 0;
            int n1 = 0;
            int dls = s.Length-1;
            int dls1 = s1.Length - 1;

            while(s[i]=='0')
            {
                i++;
            }

            while (s1[j] == '0'&& s1.Length-1>0)
            {
                j++;
            }
            n = dls - i;
            n1= dls1 - j;

            string rez = "equally";
            if (s == s1)
            {
                return rez;
            }
            if (n>n1)
            {
                rez = "more";
            }
            if (n < n1)
            {
                rez = "less";
            }
            if (n == n1)
            {
                while (true)
                {
                    if (s[i] > s1[j])
                    {
                        rez = "more";
                        break;
                    }
                    if (s[i] < s1[j])
                    {
                        rez = "less";
                        break;
                    }
                    i++;
                    j++;
                }
            }
           return rez;
        }

        public string Zeros(string s)
        {
            int i = 0;
            string rez = "true";
            int n = s.Length;

            while (rez == "true" && n > 0)
            {
                if (s[i] == '0')
                {
                    i++;
                    n--;
                }
                else
                {
                    rez = "folse";
                }
            }
          return rez;
        }

        public string RemoverZeros(string s)
        {
            int i = 0;
            int j = 0;
            int n = s.Length;
            string rez = "true";
            List<char> list = new List<char>();
            while (rez == "true" && n > 0)
            {
                if (s[i] == '0')
                {
                    i++;
                    n--;
                }
                else
                {
                    rez = "folse";
                }
            }
            if (rez == "folse")
            {
                while (n > 0)
                {

                    list.Add(s[i]);
                    j++;
                    i++;
                    n--;
                }
            }

            var ar = list.ToArray();
            string b = new string(ar);

            return b;
        }

    }
}
