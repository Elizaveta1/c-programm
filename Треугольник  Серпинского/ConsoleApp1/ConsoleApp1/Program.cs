﻿using Microsoft.SmallBasic.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
           
                GraphicsWindow.Width = 1800;
                GraphicsWindow.Height = 900;
                GraphicsWindow.Top = 20;
                GraphicsWindow.Left = 20;
                Serpinskogo(600, 400, 800, 400, 700, 600, 5);
                GraphicsWindow.Show();
                Console.ReadKey();
            }

            static void Serpinskogo(int x1, int y1, int x2, int y2, int x3, int y3, int n)
            {
                if (n == 0) return;
                GraphicsWindow.DrawTriangle(x1, y1, x2, y2, x3, y3);
                int newX1 = (x3 + x1) / 2;
                int newY1 = y1 - (y3 - y1) / 2;

                int newX2 = (x3 + x2) / 2;
                int newY2 = newY1;

                int newX3 = (x1 + x2) / 2;
                int newY3 = y1;
                //Console.ReadKey(); 

                Serpinskogo(newX1, newY1, newX2, newY2, newX3, newY3, n - 1);
                newX1 = (x3 + x1) / 2 - (x2 - x1) / 2;
                newY1 = (y1 + y3) / 2;

                newX2 = (x3 + x1) / 2;
                newY2 = (y2 + y3) / 2;

                newX3 = x1;
                newY3 = y3;

                Serpinskogo(newX1, newY1, newX2, newY2, newX3, newY3, n - 1);

                newX1 = (x3 + x2) / 2;
                newY1 = (y1 + y3) / 2;

                newX2 = (x3 + x2) / 2 + (x2 - x1) / 2;
                newY2 = (y2 + y3) / 2;

                newX3 = x2;
                newY3 = y3;
                Serpinskogo(newX1, newY1, newX2, newY2, newX3, newY3, n - 1);
        }
    }
}
