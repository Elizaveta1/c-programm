﻿/* Структура дерева находиться в файле tree.jpg*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            TreeNode p1 = new TreeNode();
            TreeNode p2 = new TreeNode();
            TreeNode p3 = new TreeNode();
            TreeNode p4 = new TreeNode();
            TreeNode p5 = new TreeNode();
            TreeNode p6 = new TreeNode();
            TreeNode p7 = new TreeNode();
            TreeNode p8 = new TreeNode();

            p1.X = 1;
            p1.Next_Left = p2;
            p1.Next_Right = p3;

            p2.X = 5;
            p2.Next_Left = p4;
            p2.Next_Right = p5;

            p4.X = 9;
            p5.X = 3;

            p3.X = 7;
            p3.Next_Left = null;
            p3.Next_Right = p6;

            p6.X = 8;
            p6.Next_Left = p7;
            p6.Next_Right = p8;

            p7.X = 5;
            p8.X = 4;

            mesTree(p1);
            Console.ReadKey();
        }
        static void mesTree(TreeNode p)
        {
            if (p != null)
            {
                TreeNode p1 = p;
                TreeNode p2 = p.Next_Left;
                mesTree(p2);
                TreeNode p3 = p.Next_Right;
                mesTree(p3);
                Console.Write(p.X);
            }

            else
                return;
        }

        class TreeNode
        {
            public int X;
            public TreeNode Next_Left;
            public TreeNode Next_Right;
        }
    }
    
}
