﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleApp1
{
    public class Class1
    {
        public int Age { get; set; }
        public bool Male { get; set; }
        public string Name { get; set; }
        public void Save (string fileName)
        {
            using(FileStream stream = new FileStream(fileName,FileMode.Create) )
            {
                var XML = new XmlSerializer(typeof(Class1));
                XML.Serialize(stream, this);

            }
        }
    }
}
